<?php
$storeIds = [2794, 2276, 1038, 1454, 1763];
$dans = [595420,708997,137425,28171,485698,799358,863567,452740,610544,846857,709006,452753,
		879536,452744,485695,853483,594080,504606,593761,525943,842480,535981,127048,524535];
$dansMehl = [488334, 468120, 706590, 531500,468121,459912,468178];
//$dans = $dansMehl;
$globalStock = 0;

// Stock
$url = 'https://products.dm.de/store-availability/DE/availability?dans='.join(',', $dans).'&storeNumbers='.join(',', $storeIds);
$jsonStock = json_decode(file_get_contents($url), true);

foreach ($storeIds as $storeId) {
	// Address
	$url = 'https://store-data-service.services.dmtech.com/stores/item/de/' . $storeId;
	$json = json_decode(file_get_contents($url), true);
	$address = sprintf('%s, %s %s', 
		$json['address']['street'],
		$json['address']['zip'],
		$json['address']['city']
	);

	$stock = 0;
	foreach ($jsonStock['storeAvailabilities'] as $entry) {
		foreach ($entry as $stockdetail) {
			if($stockdetail['store']['storeNumber'] == $storeId) {
				$stock += $stockdetail['stockLevel'];	
			}
		}
	}

	echo $address . ': ' . $stock . PHP_EOL;	
	$globalStock += $stock;
}

echo "Stock (combined): " . $globalStock . PHP_EOL;
